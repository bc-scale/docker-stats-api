# docker-stats-api

## Try it locally

```
# Start the container
docker run --rm -it --init -p "8080:8080" -v "/var/run/docker.sock:/var/run/docker.sock" registry.gitlab.com/sbenv/veroxis/docker-stats-api:latest

# query it
curl localhost:8080/api/json
```

`docker stats --all --no-trunc --format "{{ json . }}" --no-stream | jq --slurp '.'`
