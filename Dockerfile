FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.16 as builder

COPY . /app
WORKDIR /app
RUN cp "target/$(uname -m)-unknown-linux-musl/release/docker-stats-api" "/usr/local/bin/docker-stats-api"

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.16

RUN apk add --no-cache dumb-init

COPY --from=registry.gitlab.com/sbenv/veroxis/images/docker:20.10.21 "/usr/local/bin/docker" "/usr/local/bin/docker"
COPY --from=builder "/usr/local/bin/docker-stats-api" "/usr/local/bin/docker-stats-api"

ENTRYPOINT ["/usr/bin/dumb-init"]
CMD ["/usr/local/bin/docker-stats-api"]
