## [1.5.1](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.5.0...v1.5.1) (2022-09-02)


### Bug Fixes

* **sysinfo:** remove conversion from kb to bytes because sysinfo reports bytes as of 0.26.0 ([b7ba86d](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/b7ba86d618877f250a12b2e5b450a3b009741c35))

# [1.5.0](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.4.1...v1.5.0) (2022-08-15)


### Features

* add release for arm64 architecture ([8723737](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/87237372f57cc98af0b83d707a9e00ae822725a9))

## [1.4.1](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.4.0...v1.4.1) (2022-07-21)


### Bug Fixes

* remove jemalloc due to RAM usage ([422bcff](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/422bcff59c94df970c03a54b1d145adaebfba2f2))

# [1.4.0](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.3.1...v1.4.0) (2022-07-13)


### Features

* use alpine image from gitlab ([a9e0ba9](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/a9e0ba966256e44a09baf12f74f61ca6ff76126a))

## [1.3.1](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.3.0...v1.3.1) (2022-06-22)


### Bug Fixes

* remove unnecessary usage of std::sync::Arc ([e1c5fd4](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/e1c5fd4ef035dbb925e39514fd2f6439f2547046))

# [1.3.0](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.8...v1.3.0) (2022-06-20)


### Features

* allow configuring the amount of threads ([b5f9209](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/b5f92096bad816fc40ab392ead5ef80b867e1193))

## [1.2.8](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.7...v1.2.8) (2022-06-16)


### Bug Fixes

* **deps:** update rust crate sysinfo to 0.24.4 ([b383ba1](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/b383ba19dcfbaeb28882f315800fc85d90828ed4))

## [1.2.7](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.6...v1.2.7) (2022-06-16)


### Bug Fixes

* decrease image size using upx and strip ([0ae8528](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/0ae8528c329b6293daca6a02f75c84b696fd9de0))

## [1.2.6](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.5...v1.2.6) (2022-06-16)


### Bug Fixes

* use `dumb-init` to forward signals correctly ([8cbe570](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/8cbe5703f3145d863a0b440dfc57394959f059a1))

## [1.2.5](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.4...v1.2.5) (2022-06-10)


### Bug Fixes

* **deps:** update rust crate sysinfo to 0.24.3 ([616ba6e](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/616ba6e6b0f41368ad20add16003d95ca4f3c82e))

## [1.2.4](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.3...v1.2.4) (2022-06-08)


### Bug Fixes

* **deps:** update rust crate tracing to 0.1.35 ([3733661](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/373366101bdf668c8eca5529619bc2eb62c90826))

## [1.2.3](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.2...v1.2.3) (2022-06-08)


### Bug Fixes

* **deps:** update rust crate axum to 0.5.7 ([d323c48](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/d323c48401dcae7bf9a26097a71126c184b60425))

## [1.2.2](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.1...v1.2.2) (2022-06-08)


### Bug Fixes

* remove ids from container identifiers to counter recreated containers ([5d6e1f1](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/5d6e1f1e0101c90e07080f9e07a2a3dcc9dad0b2))

## [1.2.1](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.2.0...v1.2.1) (2022-06-08)


### Bug Fixes

* remove pids from container identifiers ([3d39149](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/3d391499efbdc133088f63700827548d1c0b444b))
* rename metrics ([fd0a57d](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/fd0a57debee65145d503e6db0484398dc742a767))

# [1.2.0](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.1.0...v1.2.0) (2022-06-08)


### Features

* add configuration options through env vars ([a616495](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/a616495444cfef0be2fee17e90d6dfa71b402dcf))

# [1.1.0](https://gitlab.com/sbenv/veroxis/docker-stats-api/compare/v1.0.0...v1.1.0) (2022-06-07)


### Features

* implemented prometheus data output ([4c37dec](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/4c37decff29ab48b68e5b46add4119f6f123d5ec))

# 1.0.0 (2022-06-07)


### Features

* initial commit ([4aa3126](https://gitlab.com/sbenv/veroxis/docker-stats-api/commit/4aa3126e919c001e94c3f6f9608f0f5904e27fbe))
