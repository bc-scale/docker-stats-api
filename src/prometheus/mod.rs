use std::collections::HashMap;

use prometheus::Gauge;
use prometheus::Opts;
use prometheus::Registry;

pub trait PrometheusRegistrySource {
    fn to_registry(&self, registry: &mut Registry);
}

pub fn gauge_set(
    registry: &mut Registry,
    name: &str,
    help: &str,
    labels: HashMap<String, String>,
    value: f64,
) {
    let opts = Opts::new(name, help).const_labels(labels);
    match Gauge::with_opts(opts) {
        Ok(gauge) => {
            gauge.set(value);
            if let Err(err) = registry.register(Box::new(gauge)) {
                tracing::warn!("failed to register metric: {}", err);
            }
        }
        Err(error) => tracing::warn!("failed to create gauge: {}", error),
    }
}
