use std::fmt::Display;
use std::str::FromStr;

lazy_static::lazy_static!(
    pub static ref TOKIO_THREADS: usize = read_config_env("DOCKER_STATS_API_THREADS", 4);
    pub static ref SERVER_PORT: u16 = read_config_env("DOCKER_STATS_API_PORT", 8080);
    pub static ref UPDATE_INTERVAL: u64 = read_config_env("DOCKER_STATS_API_REFRESH_INTERVAL", 15);
);

fn read_config_env<T: FromStr + Display + Clone>(env_name: &str, default: T) -> T {
    let result = match std::env::var(env_name) {
        Ok(interval) => match interval.parse::<T>() {
            Ok(interval) => interval,
            Err(_) => default.clone(),
        },
        Err(_) => default.clone(),
    };
    tracing::info!("{env_name}(default: {default}): {result}");
    result
}
