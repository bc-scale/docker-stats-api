use docker_stats_api::config::TOKIO_THREADS;
use docker_stats_api::config::UPDATE_INTERVAL;
use docker_stats_api::data_aggregator;
use docker_stats_api::docker_sys;
use docker_stats_api::sys;
use docker_stats_api::webserver;
use thiserror::Error;
use tokio::runtime::Builder;

#[derive(Debug, Error)]
pub enum ApplicationError {
    #[error("DockerStatsExecFailed: `{0}`")]
    ColorEyreInitFailed(color_eyre::eyre::ErrReport),

    #[error("TokioRuntimeFailed: `{0}`")]
    TokioRuntimeFailed(std::io::Error),

    #[error("ApplicationRunFailed: `{0}`")]
    ApplicationRunFailed(std::io::Error),
}

fn main() -> Result<(), ApplicationError> {
    init_logging();

    lazy_static::initialize(&TOKIO_THREADS);
    // Create the runtime
    let runtime = Builder::new_multi_thread()
        .worker_threads(*TOKIO_THREADS)
        .enable_io()
        .enable_time()
        .build()
        .map_err(|error| ApplicationError::TokioRuntimeFailed(error))?;

    // Spawn the root task
    runtime.block_on(async {
        run().await.unwrap();
    });

    Ok(())
}

fn init_logging() {
    #[cfg(debug_assertions)]
    {
        if std::env::var("RUST_LIB_BACKTRACE").is_err() {
            std::env::set_var("RUST_LIB_BACKTRACE", "1")
        }
        if std::env::var("RUST_BACKTRACE").is_err() {
            std::env::set_var("RUST_BACKTRACE", "full")
        }
    }

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }

    tracing_subscriber::fmt::init();
}

async fn run() -> Result<(), ApplicationError> {
    color_eyre::install().map_err(ApplicationError::ColorEyreInitFailed)?;

    lazy_static::initialize(&UPDATE_INTERVAL);

    tokio::join!(
        docker_sys::run(),
        sys::run(),
        data_aggregator::run(),
        webserver::run()
    );

    Ok(())
}
