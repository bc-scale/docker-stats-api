mod api_json;
pub mod api_prometheus;

use std::net::SocketAddr;

use api_json::api_json;
use api_prometheus::api_prometheus;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::routing::post;
use axum::Router;
use serde_json::json;

use crate::config::SERVER_PORT;

pub async fn run() {
    let router = Router::new()
        .route("/api/health", post(api_healthcheck))
        .route("/api/health", get(api_healthcheck))
        .route("/api/json", post(api_json))
        .route("/api/json", get(api_json))
        .route("/api/prometheus", post(api_prometheus))
        .route("/api/prometheus", get(api_prometheus));
    let address = SocketAddr::from(([0, 0, 0, 0], *SERVER_PORT));
    axum::Server::bind(&address)
        .serve(router.into_make_service())
        .await
        .expect("failed to serve webserver");
}

async fn api_healthcheck() -> impl IntoResponse {
    (StatusCode::OK, json!({"status": "healthy"}).to_string())
}
