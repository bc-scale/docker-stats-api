use std::process::Stdio;

use serde::Deserialize;
use serde::Serialize;
use thiserror::Error;
use tokio::process::Command;
use tokio::sync::Mutex;
use tokio::time::sleep;
use tokio::time::Duration;

use crate::config::UPDATE_INTERVAL;

lazy_static::lazy_static!(
    static ref DATA_CACHE: Mutex<Vec<DockerStatEntry>> = Mutex::new(vec![]);
);

#[derive(Debug, Error)]
pub enum DockerSysError {
    #[error("DockerStatsExecFailed")]
    DockerStatsExecFailed,

    #[error("DockerStatsExecFailedWithIoError: `{0}`")]
    DockerStatsExecFailedWithIoError(std::io::Error),

    #[error("DockerStatsReadOutputError: `{0}`")]
    DockerStatsReadOutputError(std::io::Error),

    #[error("DockerStatsNotJson: `{0}`")]
    DockerStatsNotJson(serde_json::Error),
}

pub async fn run() {
    lazy_static::initialize(&DATA_CACHE);
    loop {
        update_docker_stat_metrics().await;
        sleep(Duration::from_secs(*UPDATE_INTERVAL)).await;
    }
}

async fn update_docker_stat_metrics() {
    match query_docker_stats().await {
        Ok(data) => {
            let mut data_cache = (*DATA_CACHE).lock().await;
            *data_cache = data;
        }
        Err(err) => {
            tracing::error!("failed to updata docker_sys data: {:?}", err);
        }
    };
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DockerStatEntry {
    #[serde(rename = "BlockIO")]
    pub block_io: String,
    #[serde(rename = "CPUPerc")]
    pub cpu_perc: String,
    #[serde(rename = "Container")]
    pub container: String,
    #[serde(rename = "ID")]
    pub id: String,
    #[serde(rename = "MemPerc")]
    pub mem_perc: String,
    #[serde(rename = "MemUsage")]
    pub mem_usage: String,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "NetIO")]
    pub net_io: String,
    #[serde(rename = "PIDs")]
    pub pids: String,
}

pub async fn get_docker_stat_entries() -> Vec<DockerStatEntry> {
    (*DATA_CACHE).lock().await.clone()
}

async fn query_docker_stats() -> Result<Vec<DockerStatEntry>, DockerSysError> {
    let child_process = Command::new("docker")
        .args(&[
            "stats",
            "--no-trunc",
            "--format",
            "{{ json . }}",
            "--no-stream",
        ])
        .stdin(Stdio::null())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .map_err(DockerSysError::DockerStatsExecFailedWithIoError)?;
    let output = child_process
        .wait_with_output()
        .await
        .map_err(DockerSysError::DockerStatsReadOutputError)?;
    if !output.status.success() {
        return Err(DockerSysError::DockerStatsExecFailed);
    }
    let stdout = String::from_utf8_lossy(&output.stdout);
    let mut entries: Vec<DockerStatEntry> = vec![];
    for line in stdout.lines() {
        match serde_json::from_str::<DockerStatEntry>(line) {
            Ok(value) => {
                entries.push(value);
            }
            Err(err) => {
                return Err(DockerSysError::DockerStatsNotJson(err));
            }
        };
    }
    Ok(entries)
}
